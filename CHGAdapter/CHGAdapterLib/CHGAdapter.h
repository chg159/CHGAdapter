//
//  CHGAdapter.h
//  CHGAdapter
//
//  Created by chen haiGang on 2018/8/6.
//  Copyright © 2018年 chen haiGang. All rights reserved.
//

//#ifndef CHGAdapter_h
//#define CHGAdapter_h
//
//
//
//#endif /* CHGAdapter_h */

#import "CHGTableViewAdapter.h"
#import "UITableView+CHGSimpleTableViewAdapter.h"
#import "UICollectionView+CHGSimpleCollectionViewAdapter.h"
#import "CHGCollectionViewAdapter.h"
#import "CHGSimpleTableViewAdapter.h"
#import "CHGTableViewHeaderFooterView.h"
#import "CHGTableViewCell.h"
#import "CHGCollectionViewCell.h"

